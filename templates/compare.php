<?php
script('gpxpod', 'd3.min');
script('gpxpod', 'leaflet');
script('gpxpod', 'L.Control.MousePosition');
script('gpxpod', 'Control.Geocoder');
script('gpxpod', 'ActiveLayers');
script('gpxpod', 'Control.MiniMap');
script('gpxpod', 'L.Control.Locate.min');
script('gpxpod', 'leaflet-sidebar.min');
script('gpxpod', 'leaflet.markercluster-src');
script('gpxpod', 'Leaflet.Elevation-0.0.2.min');
script('gpxpod', 'jquery-ui.min');
script('gpxpod', 'jquery.mousewheel');
script('gpxpod', 'tablesorter/jquery.tablesorter');
script('gpxpod', 'detect_timezone');
script('gpxpod', 'jquery.detect_timezone');
script('gpxpod', 'moment-timezone-with-data.min');
script('gpxpod', 'gpxvcomp');

style('gpxpod', 'style');
style('gpxpod', 'leaflet');
style('gpxpod', 'L.Control.MousePosition');
style('gpxpod', 'Control.Geocoder');
style('gpxpod', 'leaflet-sidebar.min');
style('gpxpod', 'Control.MiniMap');
style('gpxpod', 'jquery-ui.min');
style('gpxpod', 'font-awesome.min');
style('gpxpod', 'Leaflet.Elevation-0.0.2');
style('gpxpod', 'gpxvcomp');
style('gpxpod', 'MarkerCluster');
style('gpxpod', 'MarkerCluster.Default');
style('gpxpod', 'L.Control.Locate.min');
style('gpxpod', 'tablesorter/themes/blue/style');

?>

<div id="app">
    <div id="app-content">
        <div id="app-content-wrapper">
            <?php print_unescaped($this->inc('gpxvcompcontent')); ?>
        </div>
    </div>
</div>
